#define _GNU_SOURCE
#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#include "../utils.h"
#include "../commands.h"

/** Size of a packet to read and send */
#define BUF_SIZE 256
/** Max nb of pending connect requests */
#define MAX_PENDING 256
/** Max name for a file */
#define MAX_NAME 256

#define FILES_PATH "/files"
/* Color to print server pid */
#define S_COL(str) "\033[38;2;0;255;0m"str"\033[0m"

// TODO: put in another server
void handle_client(int socket_client) {
    char command_recieved[256];
    int file;
    while (1) {
        memset(command_recieved, 0, sizeof command_recieved);
        size_t nread;
        nread = read(socket_client, command_recieved, sizeof command_recieved);
        if(nread <= 0) {
            printf(S_COL("%d")": Client exited\n", getpid());
            return;
        }
        printf(S_COL("%i: CMD:")" '%s'\n", getpid(), command_recieved);
/*PUT*/ if (strncmp(command_recieved, CMD_PUT, strlen(CMD_PUT)) == 0) { // case put
            write(socket_client, "ok", 2);
            switch (cmd_recieve_file(socket_client)) {
                case C_SUCESS:  printf("New file dowloaded\n"); break;
                case C_UNKNOWN: printf("Unknown error, coudn't get the file\n"); break;
                case C_T_ERR:   printf("Transmission error\n"); break;
                case C_404:     printf("The client is a bit dumb and the file doesn't exists on his end\n"); break;
                case C_FAE:     printf("Canceled, the dile is already here\n"); break;
                default:        printf("TODO: Other\n");
            }
/*LIST*/} else if (strncmp(command_recieved, CMD_LIST, strlen(CMD_LIST)) == 0) { // case list
            if(!list_dir(".", socket_client))
                write(socket_client, "No files on the server\n", 23);
            write(socket_client, END , CONST_SIZE); // say to the client we finished sending data

        
/*GET*/ } else if (strncmp(command_recieved, CMD_GET, strlen(CMD_GET)) == 0) { // case get
            write(socket_client, "ok", 2);
            args _args;
            if (split(command_recieved, &_args) == -1) { // shoudn't happend -> client handle directely wrong cmd
                continue;
            } else if (_args.count >= 2) {
                switch (cmd_send_file(_args.params[1], NULL, socket_client)) {
                    case C_SUCESS:  printf("File sucessfully sent : %s\n", _args.params[1]); break;
                    case C_UNKNOWN: printf("Unknown error, coudn't send file\n"); break;
                    case C_T_ERR:   printf("Transmission error\n"); break;
                    case C_404:     printf("This file '%s' doesn't exist\n", _args.params[1]); break;
                    case C_FAE:     printf("Client canceled, he already has the file\n"); break;
                    default:        printf("TODO: Other\n");
                }
            }
            free_args(&_args);
/*UNDEF*/}else { // case undefined command
            write(socket_client, "Undefined command\n", 21);
            write(socket_client, END, CONST_SIZE);
        }
        printf(S_COL("%i")": cmd handled\n", getpid());
    }
    close(socket_client);
}

int main(int argc, char *argv[]) {
    int client_sock;
    if (argc < 2 || (client_sock = atoi(argv[1])) < 0) {
        fprintf(stderr, "This server needs a valid socket\n");
        return EXIT_FAILURE;
    }

    /** Change dir to where the files are */
    char serv_path[strlen(argv[0]) + 1 + strlen(FILES_PATH)];
    strcpy(serv_path, argv[0]);
    char * w = strrchr(serv_path, '/');
    if (w) w[0] = 0;
    else strcpy(serv_path, "./");
    chdir(strcat(serv_path, FILES_PATH));

    handle_client(client_sock);
    close(client_sock);
    return EXIT_SUCCESS;
}