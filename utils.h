#ifndef MY_UTILS_H
#define MY_UTILS_H

#ifndef PATH_MAX
#define PATH_MAX 4096
#endif

typedef struct  {
    int count;
    char *cmd;
    char **params;
} args;
int list_dir (const char *dir_name, int fd);
int send_file(int from, int to);
int recieve_file(int from, int to, ssize_t file_size);

int split(const char *line, args* _args);
void free_args(args* _args);


#endif