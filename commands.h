#ifndef COMMANDS_H
#define COMMANDS_H

#define CMD_PUT "put"
#define CMD_LIST "list"
#define CMD_GET "get"
#define CMD_EXIT "exit"

// constants 

#define END "END"

/** Size of all the constants messages */
#define CONST_SIZE 3
/* Error code for sending and recieving */
/* File already present */
#define C_FAE 700
/* File not found */ 
#define C_404 404
/* Case sucess */
#define C_SUCESS 0
/* Case it's fine go */
#define C_PROCEED 1
/* Case unknown error */
#define C_UNKNOWN -1
/* Case error in the cmd */
#define C_CMD_ERR 400
/* Case not enough args */
#define C_NEA 500
/* Case transission error */
#define C_T_ERR 600
 
/** Send error code */
#define SEND_CODE(sock, err)        \
    do {                            \
    char msg[CONST_SIZE + 1];       \
    snprintf(msg, CONST_SIZE + 1, "%3d", err);        \
    write(sock, msg, CONST_SIZE);   \
    } while(0); 

int cmd_recieve_file(int fd_from);
int cmd_send_file(const char* _file, const char *_name, int fd_dest);

#endif