#include <stdio.h>
#include <dirent.h>   // opendir
#include <sys/stat.h> // file/dir stat
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

#include "utils.h"
#include "commands.h"

#define BUF_SIZE 256

/** List all files in dir_name on the file descriptor fd */
int list_dir (const char *dir_name, int fd) {
    size_t size = BUF_SIZE;
    size_t curent_pos = 0;
    int to_return = 0;
    DIR *d = opendir(dir_name);
    if (!d) {
        perror("opendir");
        switch (errno) {
            case ENOTDIR: // case it's directly a file
                write(fd, dir_name, strlen(dir_name));
                return 1;
            default: 
                SEND_CODE(fd, C_UNKNOWN);
        }
        return 0;
    }
    struct dirent *entry; // entry
    const char *d_name;   // entry name
      
    // while no error or no more entry
    while( (entry = readdir(d)) != NULL ) {
        d_name = entry->d_name; 
        // ignore when  '.' ou '..' #infinite recursion
        if (entry->d_name[0] == '.' && (entry->d_name[1] == '\0' || (entry->d_name[1] == '.' && entry->d_name[2] == 0)))
            continue;

        // path of file/dir
        char path[PATH_MAX];
        int path_length = snprintf(path, PATH_MAX, "%s/%s", dir_name, d_name);
            
        if (entry->d_type & DT_DIR){
            if (path_length > PATH_MAX) {
                fprintf(stderr, "Path length has got too long ignoring entry: %i\n", (path_length));
                continue;
            }
            //list_dir(path); // recursivity if entry is a dir
        } else {
            //print_stat(path); // print the stat (case it's not a dir)
        }
        write(fd, path, strlen(path));  
        write(fd, "\n", 1);
        to_return++;
    }
    // close dir
    if (closedir(d) == -1) {
        fprintf(stderr, "Could not close '%s': %s\n", dir_name, strerror(errno));
    }
    return to_return;
}

/** 
 * Copy all the bytes in `from` and sent them to `to` 
 * from and to are file descriptor (or socket)
 * return 0 on sucess, somthing else on failure
*/
int send_file(int from, int to) {
    char buf[BUF_SIZE];
    ssize_t nread;

    while( nread = read(from, buf, sizeof buf), nread > 0 ) {
        char *out_ptr = buf;
        ssize_t nwritten; 
        do {
            nwritten = write(to, out_ptr, nread);
            if (nwritten >= 0) {
                nread -= nwritten;
                out_ptr += nwritten;
            } else if (errno != EINTR) {
                perror("write");
                return -1;
            }
        } while (nread > 0);
    }
    return nread;
}

/** 
 * Recieve a file, until file_size bytes are written
 * Copy all the bytes in `from` and write them to `to` 
 * from and to are file descriptor (or socket)
 * return 0 on sucess, somthing else on failure
*/
int recieve_file(int from, int to, ssize_t file_size) {
    char buf[BUF_SIZE];
    ssize_t nread, total_read = 0;

    while( nread = read(from, buf, sizeof buf), nread > 0 ) {
        total_read += nread;
        char *out_ptr = buf;
        ssize_t nwritten; 
        if (total_read > file_size) // supress extra bytes ( the end msg )
            nread -= (total_read - file_size);
        do {
            nwritten = write(to, out_ptr, nread);
            if (nwritten >= 0) {
                nread -= nwritten;
                out_ptr += nwritten;
            } else if (errno != EINTR) {
                perror("write");
                return -1;
            }
        } while (nread > 0);
        if (total_read >= file_size) break;
    }
    return nread;
}

/** 
 * split string with tocke " "
 * return 0 on sucess and -1 on error
*/
int split(const char *line, args* _args){
    _args->cmd = malloc(sizeof(char) * (strlen(line) + 1));
    
    strcpy(_args->cmd, line);
    _args->count = 0;
    _args->params = malloc(sizeof(char*) );

    char *saveptr;
    char *arg = strtok_r(_args->cmd, " ", &saveptr);
    if (arg == NULL) {
        printf("NULL\n");
        free_args(_args);
        return -1;
    }
    for (; arg != NULL; arg = strtok_r(NULL, " ", &saveptr)){
        _args->params = realloc(_args->params, sizeof(char*) * (_args->count + 1));
        _args->params[_args->count] = arg;
        _args->count++;
    }
    return 0;
}

void free_args(args* _args){
    free(_args->cmd);
    free(_args->params);
}

