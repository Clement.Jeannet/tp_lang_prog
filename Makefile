CC=gcc-7
CCFLAGS=-Wall -std=c11
LDFLAGS=
SOURCES_COMMON=$(wildcard ./*.c)
SOURCES_CLIENT=$(SOURCES_COMMON) $(wildcard ./client/*.c)
SOURCES_SERVER=$(SOURCES_COMMON) $(wildcard ./server/*.c)
SOURCES_LIGHT_SERVER=$(SOURCES_COMMON) $(wildcard ./light-server/*.c)
OBJECTS_CLIENT=$(SOURCES_CLIENT:.c=.o)
OBJECTS_SERVER=$(SOURCES_SERVER:.c=.o)
OBJECTS_LIGHT_SERVER=$(SOURCES_LIGHT_SERVER:.c=.o)
TARGET_SERVER=server/server
TARGET_CLIENT=client/client
TARGET_LIGHT_SERVER=light-server/light-server
TARNAME=clement_jeannet_
CURRENTDIRNAME=TP-C
PARAMS=

all: $(TARGET_SERVER) $(TARGET_CLIENT) $(TARGET_LIGHT_SERVER)

$(TARGET_SERVER): $(OBJECTS_SERVER)
	$(CC) -o $@ $^ $(LDFLAGS) 

$(TARGET_CLIENT): $(OBJECTS_CLIENT)
	$(CC) -o $@ $^ $(LDFLAGS) 

$(TARGET_LIGHT_SERVER): $(OBJECTS_LIGHT_SERVER)
	$(CC) -o $@ $^ $(LDFLAGS) 

*%.o: %.c %.h
	$(CC) $(CCFLAGS) -c $<	

*%.o: %.c
	$(CC) $(CCFLAGS) -c $<

clean:
	rm -f ./*.o $(TARGET)

tar: all
	tar -czvf $(TARNAME)$(CURRENTDIRNAME).tar.gz $(wildcard */*.c */*.h *.c *.h Makefile *.md server/files)

valgrind: all
	valgrind --tool=memcheck --leak-check=yes --show-reachable=yes --track-origins=yes $(TARGET_CLIENT)

run: all
	./$(TARGET_SERVER)
	./$(TARGET_CLIENT)

wc:
	wc -l */*.c */*.h *.c *.h

sha1sum:
	@if [ -f ../TP3/tp3 ];then	\
		../TP3/tp3 -f *.h *.c;	\
	else						\
		sha1sum *.h *.c;		\
	fi

cd:
	cd ~/cours/Language\ de\ prog/tp