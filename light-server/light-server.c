#define _GNU_SOURCE
#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/wait.h>


/** Max nb of pending connect requests */
#define MAX_PENDING 256
/* ANSI color for the light-server */
#define LS_COL(str) "\033[38;2;255;255;0m"str"\033[0m"


/** Create a socket with port `port` */
int create_socket(int port) {
    /** socket creation */
    int sock = socket(PF_INET, SOCK_STREAM, 0);
    if(sock < 0) {
        perror("Socket");
        exit(EXIT_FAILURE);
    }

    /** adresse creation */
    struct sockaddr_in address;
    
    memset(&address, 0, sizeof address);
    address.sin_family = AF_INET; 
    address.sin_addr.s_addr = htonl(INADDR_ANY);
    address.sin_port = htons(port);
    
    if(bind(sock, (struct sockaddr*) &address, sizeof address ) < 0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }
    if (listen(sock, MAX_PENDING) < 0) {
        perror("listen failed");
        exit(EXIT_FAILURE);
    }
    printf( "Server running [ip:port] : [%s:%d]\n", inet_ntoa(address.sin_addr), port);
    return sock;
}

/** Main loop accept connection and execvp a server */
void run(int socket_server) {
    while(1) {
        struct sockaddr_in client_address;
        memset(&client_address, 0, sizeof client_address);
        printf( LS_COL("Light-server '%d'")": Waiting for incoming connections\n", getpid());
        socklen_t address_len = sizeof client_address;
        int socket_client = accept(socket_server, (struct sockaddr *) &client_address, &address_len );
        if (socket_client < 0 && errno == EINTR) continue; // continue to listen if a signal is caught
        
        pid_t pid = fork();
        if (pid < 0) { // case error
            perror(LS_COL("fork"));
        } else if (pid == 0) { // case children -> execvp server
            if ( socket_client < 0 ) {
                fprintf(stderr, LS_COL("Failed to accept client connection")": %s\n", strerror(errno));
                exit(EXIT_FAILURE);
            }
            printf(LS_COL("%d: Client connected")": %s\n", getpid(), inet_ntoa(client_address.sin_addr));
            
            char **argv = malloc(3 * sizeof(char*));
            argv[0] = "../server/server";
            argv[1] = malloc(12);
            snprintf(argv[1], 12, "%i", socket_client);
            argv[2] = NULL;
            if (execvp(argv[0], argv)) 
                fprintf(stderr, LS_COL("execvp error")": %s\n", strerror(errno));

            free(argv[1]);
            free(argv);
            exit(EXIT_FAILURE);
        } 
    }
}

/** Handler when a child died */
void handle_dead_child(int sig, siginfo_t* infos, void* useless) {
    wait(NULL);
    printf(LS_COL("server '%i' exited\n"), infos->si_pid);
}

int main(int argc, char *argv[]) {
    /** Change dir to where the light-server is are */
    char serv_path[strlen(argv[0]) + 1];
    strcpy(serv_path, argv[0]);
    char * w = strrchr(serv_path, '/');
    if (w) w[0] = 0;
    else strcpy(serv_path, "./");
    chdir(serv_path);

    int port = 4242;
    int socket = create_socket(port);

    // handle child death
    struct sigaction handle_children_death;
    memset(&handle_children_death, 0, sizeof(handle_children_death));
    handle_children_death.sa_sigaction = handle_dead_child;
    handle_children_death.sa_flags = SA_SIGINFO;
    sigaction(SIGCHLD, &handle_children_death, NULL);

    run(socket);

    close(socket);
    return EXIT_SUCCESS;
}