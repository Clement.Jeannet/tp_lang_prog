#include <stdio.h>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <libgen.h>   // basename and dirname

#include "utils.h"
#include "commands.h" 

/**
 * _path is the path of the file
 * _name is the name we want the file to have in the destination
 * if _name is NULL, the file will have the same name as here
 * fd_dest is the socket to send the file to
*/
int cmd_send_file(const char* _path, const char *_name, int fd_dest) {
    int file_fd;
    if ((file_fd = open(_path, O_RDONLY)) < 0){ // case error on open
        if (errno == ENOENT){
            SEND_CODE(fd_dest, C_404);
            return C_404;
        } else {
            perror("open");
            SEND_CODE(fd_dest, C_UNKNOWN);
            return C_UNKNOWN;
        }
    }
    
    char name_cpy[strlen(_name ? _name : _path) + 1];
    strcpy(name_cpy, _name ? _name : _path);
    
    int msg_len = CONST_SIZE + sizeof(char) * 2 + strlen(name_cpy) + sizeof(ssize_t) + 3;
    char to_send[msg_len];            
    snprintf(to_send, sizeof(to_send), "%d %s %ld", C_PROCEED, basename(name_cpy), lseek(file_fd, 0, SEEK_END));

    lseek(file_fd, 0, SEEK_SET);
    write(fd_dest, to_send, sizeof(to_send) + 1); // say we have the file and to be ready to recieve it and its size
    
    char buffer[CONST_SIZE];
    read(fd_dest, buffer, CONST_SIZE);
    
    int rcvd = strtol(buffer, NULL, 10);
    if (rcvd != C_PROCEED)
        return rcvd;

    // send file
    send_file(file_fd, fd_dest);
    close (file_fd);
    return C_SUCESS;
}

/**
 * fd_dest is the socket the file is comming from
 * return -1 on failure and 0 on sucess
*/
int cmd_recieve_file(int fd_from){
    char metadata[256];
    memset(metadata, 0, sizeof metadata);
    ssize_t nread = read(fd_from, metadata, sizeof metadata);// first message : STATUS FILE_NAME FILE_LENGTH

    int c = strtol(metadata, NULL, 10);
    if (c != C_PROCEED)
        return c;

    args _args;
    if (split(metadata, &_args)){
        SEND_CODE(fd_from, C_T_ERR);
        return C_T_ERR;
    } else if (_args.count < 3){
        SEND_CODE(fd_from, C_T_ERR);
        free_args(&_args);
        return C_T_ERR;
    }
    
    int fd = open(_args.params[1], O_CREAT | O_WRONLY | O_EXCL);
    if (fd < 0) {
        free_args(&_args);
        switch (errno) {
        case EEXIST:
            SEND_CODE(fd_from, C_FAE);
            return C_FAE;
        default:
            perror("open");
            SEND_CODE(fd_from, C_UNKNOWN);
            return C_UNKNOWN;
        }
    }
    SEND_CODE(fd_from, C_PROCEED);

    int r = recieve_file(fd_from, fd, strtol(_args.params[2], NULL, 10));
    free_args(&_args);
    close(fd);
    return r; 
}
