#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <wchar.h>
#include <sys/stat.h> // file/dir stat

#include "terminal.h"
#include "../commands.h"
#include "../utils.h"

#define SEND_ABORT(sock) write(sock, ABORT, CONST_SIZE)

int main(int argc, char *argv[]) {
    int port;
    char _adress[13];
    if (argc < 3 || (port = atoi(argv[2])) < 0) {
        printf("Uses : %s <adress> <port>\nNo enough arguments given so use of default 127.0.0.1:4242\n", argv[0]);
        port = 4242;
        strcpy(_adress, "127.0.0.1");
    } else {
        strncpy(_adress, argv[1], sizeof _adress);
    }

    /** Server connection */
    struct sockaddr_in address;
    memset(&address, 0, sizeof(address) ); // initialise adress to 0
	
    int a = inet_pton(AF_INET, _adress, &(address.sin_addr));
    if ( a == 0){ 
        printf("Invalide IP adress given: %s\n", _adress);
        perror("a:");
        exit(EXIT_FAILURE);
    } else if (a < 0){
        perror("inet_pton");
        exit(EXIT_FAILURE);
    }
        
	address.sin_family = AF_INET;
	address.sin_port = htons(port);
	
	/** Socket creation and commection */
	int sock = socket(PF_INET, SOCK_STREAM, 0);
	if (sock < 0) {
        perror("Socket creation failed");
        exit(EXIT_FAILURE);
    }
	if (connect(sock, (struct sockaddr *) &address, sizeof(address)) < 0 ) {
		perror("Coudn't connect to the socket");
        exit(EXIT_FAILURE);
	} else 
		printf("Connected to %s:%d : %s\n", _adress, port, strerror(errno));

    while (1) {
        wchar_t *line = get_line(L"\033[32mSend to server>\033[0m ");

        char *buf = calloc(sizeof(char), wcslen(line) * 4 + 1);
        wcstombs(buf, line, wcslen(line) * 4 + 1);

/*EXIT*/if (strcmp(buf, CMD_EXIT) == 0) {
            free(buf);
            free(line);
            break;
/*PUT*/ } else if (strncmp(buf, CMD_PUT, strlen(CMD_PUT)) == 0) { // case put
            
            args _args;
            char *arg2 = NULL;
            if (split(buf, &_args) == -1) {
                printf("Error when analysing the command\n");
                continue;
            } else if (_args.count < 2) {
                printf("Not enough arguments for put\n");
                free_args(&_args);
                continue;
            } else if (_args.count > 2) {
                arg2 = _args.params[2];
            }
            
            write(sock, buf, strlen(buf));// send cmd to serv
            char buf2[2];
            read(sock, buf2, 2);
            switch (cmd_send_file(_args.params[1], arg2, sock)) {
                case C_SUCESS:  printf("File sucessfully sent : '%s'\n", _args.params[1]); 
                    if (arg2) printf("it has been renamed '%s'\n", arg2);
                    break;
                case C_UNKNOWN: printf("Unknown error, coudn't send file\n"); break;
                case C_T_ERR:   printf("Transmission error\n"); break;
                case C_404:     printf("The file '%s' doesn't exist\n", _args.params[1]); break;
                case C_FAE:     printf("A file of the same name already exists on the server\n"); break;
                default:        printf("TODO: Other\n"); break;
            }
            
            free_args(&_args);
/*GET*/ } else if (strncmp(buf, CMD_GET, strlen(CMD_GET)) == 0) { // case get
            
            args _args;
            if (split(buf, &_args) == -1) {
                printf("Error in command get\n");
                continue;
            } else if (_args.count < 2) {
                printf("Not enough arguments for get\n");
                free_args(&_args);
                continue;
            }
            write(sock, buf, strlen(buf)); // send cmd to serv
            char buf2[2];
            read(sock, buf2, 2);
            switch (cmd_recieve_file(sock)) {
                case C_SUCESS: printf("File sucessfully got : %s\n", _args.params[1]); break;
                case C_UNKNOWN: printf("Unknown error, coudn't get file\n"); break;
                case C_T_ERR: printf("Transmission error\n"); break;
                case C_404: printf("File not present on the server\n"); break;
                case C_FAE: printf("A file of the same name already exists\n"); break;
                default: printf("TODO: Other\n");
            }
            //if (cmd_recieve_file(sock) == 0)
            //    printf("File sucessfully got : %s\n", _args.params[1]);
            free_args(&_args);

        } else if (buf[0] != 0 && buf != NULL) { // case server only cmd -> send and recieve all
            write(sock, buf, strlen(buf));
            
            char buffer[256];
            char lasts[CONST_SIZE + 1];
            memset(lasts, 0, CONST_SIZE);
            ssize_t nread;
            while (1) {
                memset(buffer, 0, sizeof buffer);
                if ( (nread = read(sock, buffer, sizeof buffer)) < 0) {
                    perror("read");
                    break;
                }
                //printf("\033[34mnread:%ld '%s'\033[0m", nread, buffer);
                if (nread > CONST_SIZE) {
                    for (int i = nread - CONST_SIZE; i < nread; i++) {
                        lasts[CONST_SIZE - nread + i] = buffer[i];
                        //printf(" \033[33m%ld->%d:%c\033[0m ",CONST_SIZE - nread + i + 1, i, buffer[i]);
                    }
                } else {
                    for (int i = 0; i < nread; i++)
                        lasts[i] = lasts[CONST_SIZE - nread + i];
                    for (int i = 0; i < nread; i++)
                        lasts[CONST_SIZE - nread + i] = buffer[i];
                    
                }
                
                if (strncmp(lasts, END, CONST_SIZE) == 0) {
                    if (nread > CONST_SIZE) {
                        buffer[nread - CONST_SIZE] = 0;
                        printf("%s", buffer);
                    }
                    break;
                }
                printf("%s", buffer);
                //printf("\033[31mlasts >'%s'\033[0m\n", lasts);
            }
            //printf("\n");
        }
        free(buf);
        free(line);
    }


    
    close(sock);
    return 0;
}
