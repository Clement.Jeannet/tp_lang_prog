#ifndef TERMINAL_H
#define TERMINAL_H

int init_terminal();

wchar_t *get_line(const wchar_t *to_print);

#endif