#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>

#include <termios.h>
#include <sys/ioctl.h>

#include <wchar.h> // utf-8
#include <locale.h> // locale

#include "terminal.h"

#ifndef HOST_NAME_MAX
#define HOST_NAME_MAX 64
#endif
#ifndef PATH_MAX
#define PATH_MAX 4096
#endif

struct termios tty_attr_old;
int old_keyboard_mode;

void reset_terminal();
int init_terminal();

/** Thanks internet 
 * change the terminal to row mode (getwchar get wchars one by one)
 * `string` will be printed before asking for a line
 * return 0 on sucess, -1 on failure
*/
int init_terminal() {
    // save old attributes
    tcgetattr(STDIN_FILENO, &tty_attr_old);
    // init our new one
    struct termios tty_attr = tty_attr_old; 
    // ICANON : disable canonical mode (allow read input byte by byte), ECHO disable auto print of chars
    // ISIG disable auto ctrl + C and ctrl + Z
    tty_attr.c_lflag   &= ~(ICANON | ECHO);    
    // IXON : disable ctrl + S and ctrl + Q
    // IEXTEN : disable ctrl + V (not used here)
    // ICRNL : ctrl + M now gives \r
    // ICRNL : now \r gives \n
    tty_attr.c_iflag   &= ~(ICRNL | IXOFF | ISTRIP);
    tty_attr.c_cflag |= (CS8); // supposed to be good
    /* block until 1 key comes in */ // dont work on window but doesn't matter for that
    //tty_attr.c_cc[VMIN] = 1;
    //printf ("cur : %d, old : %d\n", tty_attr.c_iflag, tty_attr_old.c_iflag);
    tcsetattr(STDIN_FILENO, TCSANOW, &tty_attr);
    // TODO check if mode corectely enabled
    if (!setlocale(LC_CTYPE, "")) {
        printf("Coudn't set locale\n");
        reset_terminal();
        return -1;
    }
    return 0;
}
/** Reset the terminal to it's previous state */
void reset_terminal() {
    tcsetattr(STDIN_FILENO, TCSAFLUSH, &tty_attr_old);
}

/** 
 * Scan a line from stdin, auto malloc the right size 
 * return a pointer to the wint_t* auto mallocked
*/
wchar_t *scan_line() {
    int size = 64, nwritten = 0;
    wchar_t *line = malloc(sizeof (wchar_t) * (size + 1)); 
    wint_t c;
    while ( c = getwchar(), c != '\n' && c != '\r' && c != '\033'){
        if (nwritten + 1 >= size)
            line = realloc(line, sizeof(wchar_t) * (size <<= 1 + 1)); 
        line[nwritten++] = c;
    }
    line[nwritten] = 0;
    return line;
}   

/** 
 * Get a line from stdin, in UTF-8
 * to_print will be printed before scaning for entry
 * can return NULL (like if ctrl+C)
*/
wchar_t* get_line(const wchar_t *to_print) {
    if (to_print) 
        printf("%ls", to_print);
    
    // case we are not in a fancy terminal
    if (init_terminal() == -1)
        return scan_line();
    int size = 64; // usually inputs are not really big 
    int nwritten = 0;
    wchar_t *line_buffer = calloc(sizeof (wchar_t),  ( size + 1));
    /** Current position on screen, start at 1 */
    int c_pos = 1;
    /* Rewrite the current line */

    void refresh() { // <- the linter says thats an error, but everythings work perfectely
         // printf("\033[2K\rnwritter : %d, cpos: %d: mv left: %d\n", nwritten, c_pos,  nwritten - c_pos + 1); // DEBUG
        printf("\033[2K\r%ls%ls", to_print, line_buffer); 
        if (nwritten - c_pos + 1 > 0) printf("\033[%dD", nwritten - c_pos + 1);
    };
    wint_t c;
    while (1) {
        c = getwchar();
        //printf(">>> '%d' = '%c'\n", c, c);
        zap_getwchar: if (c == '\r' || c == '\n') break;
        
        // TODO: multiline entry (with \)
        switch(c){
            case -1:  // case ctrl+C or ctrl+D
                //printf("%d: ctrl + C\n", getpid());
                free(line_buffer); 
                //printf("\n");
                reset_terminal(); 
                return NULL;
            case '\033': ;// case escape code
                //printf("esc\033[31m%d\033[0m ", c);
                // there is always a 91 '[' or a 79 (f1 to f4 ???) in case of special key
                getwchar(); // remove the char after ESC, yes even if it is a valic char, dontknow why but bash work like that
                
                /*if (c2 != 91 && c2 != 79) { // case the ESC is just pressed (doesn't work like that in bash, it just not print the next char)
                    c = c2;
                    goto zap_getwchar;
                }   */  
                //printf("\033[31m%d\033[0m ", c2);
                wint_t r = getwchar();
                //printf("\033[31m%d\033[0m ", r);
                switch(r) { // case arrows
                    case 65: break; // TODO case up
                    case 66: break;// TODO case down 
                    case 67:  // case right
                        if (c_pos <= nwritten){
                            c_pos++;
                            refresh();
                        }
                        break; // TODO case down
                    case 68: ;// case left
                        if (c_pos - 1 > 0){ 
                            c_pos--;
                            refresh();
                        }
                        break; // case right
                    // cases f1-f4
                    case 80: 
                    case 81: 
                    case 82: 
                    case 83: // remove the last part 
                        getwchar();
                        //printf("\033[31m%d\033[0m ", getwchar()); 
                        break;
                    // case f5-f12
                    case 49: 
                    case 50: // remove the last part but the 5th stay on  bash (a ~ char) so it stay here too
                        getwchar();//printf("\033[31m%d\033[0m ", getwchar());
                        //printf("\033[31m%d\033[0m ", getwchar());
                        break;
                    case 51: // case delete
                        if (getwchar() != 126 || c_pos == nwritten + 1) break;
                
                        for (int i = c_pos - 1; i < nwritten + 1; i++){
                            line_buffer[i] = line_buffer[i+1];
                        }
                        line_buffer[nwritten - 1]=0;
                        nwritten--;
                        refresh();
                        break;
                    // pgup, pgdn
                    case 53: 
                    case 54:
                        getwchar();//printf("\033[31m%d\033[0m ", getwchar());
                        break;

                    case 70 : // end TODO move cursor break;
                        break;
                    case 72 : // home TODO printf("\r"); break;
                        break;
                    default: 
                        c = r;
                        goto zap_getwchar;
                }
                break;
            case '\t': // because it's asked that \t is also a space //TODO auto completion
                c = ' ';
                goto zap_getwchar;
            case 26: //printf("\033[31m%d\033[0m ", 26); // case pause break
                break;
            case 127: ;
                if (c_pos == 1) break; // if nothing to delete
                
                for (int i=c_pos - 2; i < nwritten + 1; i++){
                    line_buffer[i] = line_buffer[i+1];
                }
                line_buffer[nwritten - 1] = 0;
                nwritten--;
                c_pos--;
                refresh();
                //printf("\033[31m%d\033[0m ", 127); 
                break; // case backspace
            default :; // normal char
            
                if (nwritten + 1 >= size) // always have enough place for the new wint_t
                    line_buffer = realloc(line_buffer, sizeof (wchar_t) * (size <<= 1 + 1)); 
                
                if (nwritten == c_pos - 1){ // default case, add the wint_t at the end
                    line_buffer[nwritten] = c;
                } else {
                    /** Insert the wint_t at the right place */
                    wchar_t last_char = c, next_char;
                    for (int i=c_pos - 1; i < nwritten + 1; i++){
                        next_char = line_buffer[i];
                        line_buffer[i] = last_char;
                        last_char = next_char;
                    }
                    line_buffer[nwritten + 2] = 0;// end of line  
                }
                nwritten++;// increase total nb chars 
                c_pos++; // increase curent position
                refresh();
                break;  
        }
    }
    printf("\n");
    reset_terminal();
    return line_buffer;
}